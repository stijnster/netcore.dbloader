using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DbLoader
{
    class FileReader
    {
        private SqlConnection _connection;

        public void Run(string fileName, string connectionString){
            OpenConnection(connectionString);
            try{
                int totalLineCount = 0;
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        totalLineCount++;
                    }
                }
                
                using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                {
                    DateTime start = DateTime.Now;
                    int lineCount = 0;
                    int blockCount = 0;
                    double progress = 0.0;
                    string line;
                    List<string> block = new List<string>();

                    while ((line = sr.ReadLine()) != null)
                    {
                        if(line.Trim().ToUpper().Equals("GO")){
                            ProcessBlock(block.ToArray());
                            blockCount++;
                            block.Clear();
                        }
                        else{
                            block.Add(line);
                        }
                        lineCount++;
                        if(lineCount % 1000 == 0)
                        {
                            double secondsSinceStart = (DateTime.Now - start).TotalSeconds;
                            progress = lineCount / (double)totalLineCount;
                            double secondsRemaining = (secondsSinceStart / progress) - secondsSinceStart;
                            Console.WriteLine(String.Format("{0}/{1} lines processed ({2}) {3}s remaining", lineCount, totalLineCount, progress.ToString("P2"), secondsRemaining.ToString("F2")));
                        }
                    }
                    Console.WriteLine(String.Format("{0} blocks processed", blockCount));
                }
            }
            finally{
                CloseConnection();
            }
        }

        private void OpenConnection(string connectionString){
            Console.WriteLine("Opening Connection");
            _connection = new SqlConnection();
            _connection.ConnectionString = connectionString;
            _connection.Open();
        }

        private void CloseConnection(){
            _connection.Close();
            _connection.Dispose();
            Console.WriteLine("Closed Connection");
        }

        private void ProcessBlock(string[] block){
            SqlCommand sqlCommand = new SqlCommand(String.Join("\n", block), _connection);
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Dispose();
        }
    }
}