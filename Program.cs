﻿using System;
using DbLoader;

namespace ConsoleApplication
{
    public class Program
    {
        public static void printHelp()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("------");
            Console.Write("\n\t <sqlfile> <connectionstring>\n\n");
            Console.WriteLine("Example");
            Console.WriteLine("\n\t  ~/Desktop/filename.sql \"User ID=aznl;Password=aznl;Persist Security Info=True;Network Address=pegasus.lan;Initial Catalog=AZNL-AAP\"");
            Console.Write("\n\n");
        }

        public static int Main(string[] args)
        {
            if(args.Length < 2)
            {
                printHelp();
                return 1;
            }

            String fileName = args[0];
            String connectionString = args[1];

            FileReader fileReader = new FileReader();
            fileReader.Run(fileName, connectionString);

            return 0;
        }
    }
}
