dbloader loads a large MS SQL Server dump file (created by the Database Migration Wizard), loads it into blocks, and executes those blocks.

run the program for debug;

    dotnet run ~/Desktop/filename.sql "Pwd=dbload;Persist Security Info=False;User ID=dbload;Initial Catalog=DBLOADER;Data Source=192.168.1.22"

compile the program for windows 7 x64;

    dotnet publish -c release -r win7-x64